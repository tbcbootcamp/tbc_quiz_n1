package com.example.tbcquiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity() : AppCompatActivity() {

    private val users = mutableListOf<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {

        addUserButton.setOnClickListener {
            if (emailEditText.text.isEmpty() || ageEditText.text.isEmpty() ||
                nameEditText.text.isEmpty() || surnameEditText.text.isEmpty()
            )
                return@setOnClickListener Toast.makeText(
                    this,
                    "შეავსეთ ყველა ველი",
                    Toast.LENGTH_SHORT
                ).show()

            if (checkUser(emailEditText.text.toString())) {
                Toast.makeText(this, "მომხმარებელი უკვე არსებობს", Toast.LENGTH_SHORT).show()
            } else {
                users.add(
                    User(
                        nameEditText.text.toString(),
                        surnameEditText.text.toString(),
                        ageEditText.text.toString().toInt(),
                        emailEditText.toString()
                    )
                )
                Toast.makeText(this, "მომხმარებელი დაემატა წარმატბით", Toast.LENGTH_SHORT).show()
            }
        }

        removeUserButton.setOnClickListener {
            if (emailEditText.text.isEmpty() || ageEditText.text.isEmpty() || nameEditText.text.isEmpty() ||
                surnameEditText.text.isEmpty()
            )
                return@setOnClickListener Toast.makeText(
                    this,
                    "შეავსეთ ყველა ველი!",
                    Toast.LENGTH_SHORT
                ).show()

            if (checkUser(emailEditText.text.toString())) {
                removeUser(emailEditText.text.toString())
                Toast.makeText(this, "მომხმარებელი წარმატებით წაიშალა", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "მომხმარებელი არ არსებობს", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun checkUser(email: String): Boolean {
        users.forEach {
            if (it.email == email) return true
        }
        return false
    }

    private fun ifUserExists(email: String): Boolean {
        for (user in users) {
            if (user.email == email) return true
        }
        return false
    }

    private fun removeUser(email: String) {
        for (user in users) {
            if (user.email == email) {
                users.remove(user)

            }
        }
    }
}
